import { CREATE, DELETE, UPDATE } from "./ActionTypes";

export const createRecord = (userDetails) => {
  return {
    type: CREATE,
    payload: userDetails,
  };
};

export const deleteRecord = (id) => {
  return {
    type: DELETE,
    payload: id,
  };
};

export const updateRecord = (userData, id) => {
  return {
    type: UPDATE,
    payload: userData,
    id: id,
  };
};
