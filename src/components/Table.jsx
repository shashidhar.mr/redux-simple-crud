import React, { useEffect, useState } from "react";
import Nav from "./Nav";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { deleteRecord } from "../redux/Action";

function Table({
  data,
  setData,
  userData,
  setUserData,
  status,
  setStatus,
  index,
  setIndex,
}) {
  const state = useSelector((state) => state.data);
  console.log(state, "stateTable");
  const dispatch = useDispatch();

  const [tableData, setTableData] = useState([]);
  useEffect(() => {
    setStatus(false);
    setUserData({
      name: "",
      qualBe: false,
      qualMtech: false,
      gender: "",
      country: "",
    });
  }, []);

  // useEffect(() => {
  //   const getTasks = JSON.parse(localStorage.getItem("taskAdded"));
  //   debugger;
  //   setTableData(getTasks);
  //   setData(getTasks);
  // }, [data]);

  // useEffect(() => {
  //   debugger;
  //   console.log(tableData);
  // }, [tableData]);

  let navigate = useNavigate();

  // useEffect(() => {
  //   debugger;
  //   console.log(data, "data");
  // }, [JSON.stringify(data)]);

  const editData = (index) => {
    setIndex(index);
    debugger;
    console.log(index);
    navigate("/form");
    debugger;
    let copy = JSON.parse(JSON.stringify(state[index]));
    setUserData(copy);
    setStatus(true);
  };

  const deleteData = (index) => {
    debugger;
    console.log(index);
    dispatch(deleteRecord(index));
    // let copy = JSON.parse(JSON.stringify(data));
    // copy.splice(index, 1);
    // debugger;
    // setData(copy);
    // localStorage.setItem("taskAdded", JSON.stringify(copy));
  };

  return (
    <div>
      <Nav />
      <table class="table">
        <tr>
          <th>SL.No</th>
          <th>Name</th>
          <th>Qualification</th>
          <th>Gender</th>
          <th>Country</th>
          <th>Actions</th>
        </tr>
        {console.log(state.length, "state.length")}
        {state.length > 0 &&
          state.map((value, index) => {
            return (
              <tr key={index}>
                <td>{parseInt(index) + 1}</td>
                <td>{value.name}</td>
                <td>
                  {value.qualBe && value.qualMtech
                    ? `BTech, MTech`
                    : (value.qualBe && "BTech") || (value.qualMtech && "MTech")}
                </td>
                <td>{value.gender}</td>
                <td>{value.country}</td>
                <td>
                  <i
                    class="fa fa-pencil"
                    aria-hidden="true"
                    onClick={() => editData(index)}
                  ></i>{" "}
                  <i
                    class="fa fa-trash"
                    aria-hidden="true"
                    onClick={() => deleteData(index)}
                  ></i>
                </td>
              </tr>
            );
          })}
      </table>
    </div>
  );
}

export default Table;
