import React from 'react'
import { Link } from 'react-router-dom'

function Nav() {
  return (
    <>
    <div class="container-fluid">
  <br/>
  <h3>Sticky Navbar</h3>
  <p>A sticky navigation bar stays fixed at the top of the page when you scroll past it.</p>
  <p>Scroll this page to see the effect. <strong>Note:</strong> sticky-top does not work in IE11 and earlier.</p>
</div>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark sticky-top">
  <Link class="navbar-brand" to="/dashboard">Logo</Link>
  <ul class="navbar-nav">
    <li class="nav-item">
      <Link class="nav-link" to="/form">Form</Link>
    </li>
    <li class="nav-item">
      <Link class="nav-link" to="/table">Table</Link>
    </li>
    
  </ul>
</nav>


    </>
  )
}

export default Nav