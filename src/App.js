import logo from "./logo.svg";
import "./App.css";
import { useSelector, useDispatch } from "react-redux";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { useState } from "react";

import Dashboard from "./components/Nav";
import Form from "./components/Form";
import Table from "./components/Table";
import Register from "./components/Register";
import Login from "./components/Login";
function App() {
  const state = useSelector((state) => state.data);
  console.log(state, "state");

  const [data, setData] = useState([]);
  const [userData, setUserData] = useState({
    name: "",
    qualBe: false,
    qualMtech: false,
    gender: "",
    country: "",
  });
  const [status, setStatus] = useState(false);
  const [index, setIndex] = useState("");

  return (
    <div className="App">
      <Router>
        <Routes>
          <Route exact path="/" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/dashboard" element={<Dashboard />} />
          <Route
            path="/form"
            element={
              <Form
                data={data}
                setData={setData}
                userData={userData}
                setUserData={setUserData}
                status={status}
                setStatus={setStatus}
                index={index}
                setIndex={setIndex}
              />
            }
          />
          <Route
            path="/table"
            element={
              <Table
                data={data}
                setData={setData}
                userData={userData}
                setUserData={setUserData}
                status={status}
                setStatus={setStatus}
                index={index}
                setIndex={setIndex}
              />
            }
          />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
