import React from 'react'
import { useNavigate } from "react-router-dom";

function Login() {
    let navigate = useNavigate();

    const login = () => {
        navigate('/dashboard');
    }

    const register  = () => {
        navigate('/');
    }
  return (
    
    <div>Login

<button onClick={login}>Login</button>
<button onClick={register}>Register</button>
    </div>
    
  )
}

export default Login