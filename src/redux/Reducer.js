import { CREATE, DELETE, UPDATE } from "./ActionTypes";

const initialState = {
  data: [],
};

function crudReducer(state = initialState, action) {
  debugger;
  switch (action.type) {
    case CREATE: {
      state.data.push(action.payload);
      debugger;
      return state;
    }

    case DELETE: {
      let obj = {};
      let data = state.data.filter((value, index) => {
        return index !== action.payload;
      });
      console.log(data, "data");
      obj.data = data;
      return obj;
    }

    case UPDATE: {
      debugger;
      state.data[action.id] = action.payload;
      return state;
    }

    default: {
      return state;
    }
  }
}

export default crudReducer;
